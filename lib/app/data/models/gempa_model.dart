// To parse this JSON data, do
//
//     final gempa = gempaFromJson(jsonString);

import 'dart:convert';

Gempa gempaFromJson(String str) => Gempa.fromJson(json.decode(str));

String gempaToJson(Gempa data) => json.encode(data.toJson());

class Gempa {
  Gempa({
    this.id,
    this.eventid,
    this.coordinate,
    this.latitude,
    this.longitude,
    this.magnitude,
    this.depth,
    this.area,
    this.potential,
    this.subject,
    this.headline,
    this.deskripsi,
    this.instruction,
    this.instruction1,
    this.instruction2,
    this.instruction3,
    this.obsarea,
    this.wzarea,
    this.shakemap,
    this.dirasakan,
    this.eventtime,
    this.timesent,
    this.createdAt,
    this.updatedAt,
    this.resourceUrl,
    this.lat,
    this.lon,
  });

  int? id;
  String? eventid;
  String? coordinate;
  String? latitude;
  String? longitude;
  String? magnitude;
  String? depth;
  String? area;
  String? potential;
  String? subject;
  String? headline;
  String? deskripsi;
  String? instruction;
  dynamic instruction1;
  dynamic instruction2;
  dynamic instruction3;
  dynamic obsarea;
  dynamic wzarea;
  String? shakemap;
  String? dirasakan;
  DateTime? eventtime;
  DateTime? timesent;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? resourceUrl;
  String? lat;
  String? lon;

  factory Gempa.fromJson(Map<String, dynamic> json) => Gempa(
        id: json["id"],
        eventid: json["eventid"],
        coordinate: json["coordinate"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        magnitude: json["magnitude"],
        depth: json["depth"],
        area: json["area"],
        potential: json["potential"],
        subject: json["subject"],
        headline: json["headline"],
        deskripsi: json["deskripsi"],
        instruction: json["instruction"],
        instruction1: json["instruction1"],
        instruction2: json["instruction2"],
        instruction3: json["instruction3"],
        obsarea: json["obsarea"],
        wzarea: json["wzarea"],
        shakemap: json["shakemap"],
        dirasakan: json["dirasakan"],
        eventtime: DateTime.parse(json["eventtime"]),
        timesent: DateTime.parse(json["timesent"]),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        resourceUrl: json["resource_url"],
        lat: json["lat"],
        lon: json["lon"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "eventid": eventid,
        "coordinate": coordinate,
        "latitude": latitude,
        "longitude": longitude,
        "magnitude": magnitude,
        "depth": depth,
        "area": area,
        "potential": potential,
        "subject": subject,
        "headline": headline,
        "deskripsi": deskripsi,
        "instruction": instruction,
        "instruction1": instruction1,
        "instruction2": instruction2,
        "instruction3": instruction3,
        "obsarea": obsarea,
        "wzarea": wzarea,
        "shakemap": shakemap,
        "dirasakan": dirasakan,
        "eventtime": eventtime!.toIso8601String(),
        "timesent": timesent!.toIso8601String(),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "resource_url": resourceUrl,
        "lat": lat,
        "lon": lon,
      };
}
