// To parse this JSON data, do
//
//     final cuaca = cuacaFromJson(jsonString);

import 'dart:convert';

Cuaca cuacaFromJson(String str) => Cuaca.fromJson(json.decode(str));

String cuacaToJson(Cuaca data) => json.encode(data.toJson());

class Cuaca {
  Cuaca({
    this.lokasi,
    this.cuaca,
  });

  Lokasi? lokasi;
  List<CuacaElement>? cuaca;

  factory Cuaca.fromJson(Map<String, dynamic> json) => Cuaca(
        lokasi: Lokasi.fromJson(json["lokasi"]),
        cuaca: List<CuacaElement>.from(
            json["cuaca"].map((x) => CuacaElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "lokasi": lokasi!.toJson(),
        "cuaca": List<dynamic>.from(cuaca!.map((x) => x.toJson())),
      };
}

class CuacaElement {
  CuacaElement({
    this.date,
    this.tmin,
    this.tmax,
    this.humin,
    this.humax,
    this.hu,
    this.t,
    this.wd,
    this.ws,
    this.weather,
    this.image,
    this.weatherDesc,
  });

  DateTime? date;
  int? tmin;
  int? tmax;
  int? humin;
  int? humax;
  int? hu;
  int? t;
  String? wd;
  String? ws;
  int? weather;
  String? image;
  String? weatherDesc;

  factory CuacaElement.fromJson(Map<String, dynamic> json) => CuacaElement(
        date: DateTime.parse(json["date"]),
        tmin: json["tmin"],
        tmax: json["tmax"],
        humin: json["humin"],
        humax: json["humax"],
        hu: json["hu"],
        t: json["t"],
        wd: json["wd"],
        ws: json["ws"],
        weather: json["weather"],
        image: json["image"],
        weatherDesc: json["weather_desc"],
      );

  Map<String, dynamic> toJson() => {
        "date": date!.toIso8601String(),
        "tmin": tmin,
        "tmax": tmax,
        "humin": humin,
        "humax": humax,
        "hu": hu,
        "t": t,
        "wd": wd,
        "ws": ws,
        "weather": weather,
        "image": image,
        "weather_desc": weatherDesc,
      };
}

class Lokasi {
  Lokasi({
    this.kecId,
    this.nama,
    this.kab,
    this.prov,
    this.jarak,
    this.lokasi,
  });

  int? kecId;
  String? nama;
  String? kab;
  String? prov;
  double? jarak;
  String? lokasi;

  factory Lokasi.fromJson(Map<String, dynamic> json) => Lokasi(
        kecId: json["kec_id"],
        nama: json["nama"],
        kab: json["kab"],
        prov: json["prov"],
        jarak: json["jarak"].toDouble(),
        lokasi: json["lokasi"],
      );

  Map<String, dynamic> toJson() => {
        "kec_id": kecId,
        "nama": nama,
        "kab": kab,
        "prov": prov,
        "jarak": jarak,
        "lokasi": lokasi,
      };
}
