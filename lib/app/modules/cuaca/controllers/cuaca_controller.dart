import 'dart:convert';

import 'package:get/get.dart';
import 'package:mhews/app/data/models/cuaca_model.dart';
import 'package:mhews/app/modules/cuaca/providers/cuaca_provider.dart';

class CuacaController extends GetxController {
  late Future<Cuaca> futureCuaca;
  CuacaProvider cuacaProvider = CuacaProvider();
  @override
  void onInit() {
    super.onInit();
    futureCuaca = cuacaProvider.fetchCuaca();
    print(futureCuaca);
  }
}
