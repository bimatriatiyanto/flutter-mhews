import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:mhews/app/data/models/cuaca_model.dart';

class CuacaProvider extends GetConnect {
  final url = "http://mhewsv2.test/api/cuaca";

  Future<Cuaca> fetchCuaca() async {
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.

      return cuacaFromJson(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Data tidak tersedia');
    }
  }
}
