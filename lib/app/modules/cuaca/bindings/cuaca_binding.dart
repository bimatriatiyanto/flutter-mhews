import 'package:get/get.dart';
import 'package:mhews/app/modules/cuaca/providers/cuaca_provider.dart';

import '../controllers/cuaca_controller.dart';

class CuacaBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CuacaController>(
      () => CuacaController(),
    );
    Get.lazyPut<CuacaProvider>(
      () => CuacaProvider(),
    );
  }
}
