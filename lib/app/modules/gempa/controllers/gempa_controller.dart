import 'package:get/get.dart';
import 'package:mhews/app/data/models/gempa_model.dart';
import 'package:mhews/app/modules/gempa/providers/gempa_provider.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:math';

class GempaController extends GetxController {
  final count = 0.obs;
  late Future<Gempa> futureGempa;
  GempaProvider gempaProvider = GempaProvider();
  double? jarak;
  var jarak1 = ''.obs;
  var latitudeGempa = ''.obs;
  var longitudeGempa = ''.obs;
  var coordinate = ''.obs;
  var latitudeDevice = ''.obs;
  var longitudeDevice = ''.obs;
  final storage = const FlutterSecureStorage();
  static final R = 6372.8; // In kilometers

  @override
  void onInit() {
    super.onInit();
    futureGempa = gempaProvider.fetchGempa();
    getData();
    getJarak();
  }

  @override
  void onReady() {
    super.onReady();
  }

  getData() async {
    latitudeDevice.value = (await storage.read(key: "latitude"))!;
    longitudeDevice.value = (await storage.read(key: "longitude"))!;
    futureGempa.then((value) async {
      coordinate.value = value.coordinate.toString();
      latitudeGempa.value = value.lat.toString();
      longitudeGempa.value = value.lon.toString();

      jarak = haversine(
          double.parse(latitudeDevice.toString()),
          double.parse(longitudeDevice.toString()),
          double.parse(latitudeGempa.toString()),
          double.parse(longitudeGempa.toString()));

      await storage.write(key: "jarak", value: jarak?.toStringAsFixed(3));
    });
  }

  static double haversine(double lat1, lon1, lat2, lon2) {
    double dLat = _toRadians(lat2 - lat1);
    double dLon = _toRadians(lon2 - lon1);
    lat1 = _toRadians(lat1);
    lat2 = _toRadians(lat2);
    double a =
        pow(sin(dLat / 2), 2) + pow(sin(dLon / 2), 2) * cos(lat1) * cos(lat2);
    double c = 2 * asin(sqrt(a));
    return R * c;
  }

  static double _toRadians(double degree) {
    return degree * pi / 180;
  }

  getJarak() async {
    jarak1.value = (await storage.read(key: "jarak"))!;
    print(jarak1);
  }
}
