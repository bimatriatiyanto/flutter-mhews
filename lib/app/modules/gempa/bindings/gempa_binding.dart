import 'package:get/get.dart';
import 'package:mhews/app/modules/gempa/providers/gempa_provider.dart';

import '../controllers/gempa_controller.dart';

class GempaBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<GempaController>(
      () => GempaController(),
    );
    Get.lazyPut<GempaProvider>(
      () => GempaProvider(),
    );
  }
}
