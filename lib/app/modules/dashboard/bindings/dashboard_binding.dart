import 'package:get/get.dart';
import 'package:mhews/app/modules/cuaca/controllers/cuaca_controller.dart';
import 'package:mhews/app/modules/gempa/controllers/gempa_controller.dart';
import 'package:mhews/app/modules/home/controllers/home_controller.dart';
import 'package:mhews/app/modules/login/controllers/login_controller.dart';

import '../controllers/dashboard_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashboardController>(
      () => DashboardController(),
    );
    Get.lazyPut<GempaController>(
      () => GempaController(),
    );
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<CuacaController>(
      () => CuacaController(),
    );
    Get.lazyPut<LoginController>(
      () => LoginController(),
    );
  }
}
