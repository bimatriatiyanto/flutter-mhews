import 'dart:async';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:mhews/app/data/service/service_location.dart';

class LoginController extends GetxController {
  var latitude = ''.obs;
  var longitude = ''.obs;
  var address = ''.obs;
  late StreamSubscription<Position> streamSubscription;
  Posisi posisi = new Posisi();
  final storage = const FlutterSecureStorage();

  @override
  void onInit() async {
    super.onInit();
    posisi.checkGps();
    getLocation();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  //Get location
  getLocation() async {
    streamSubscription =
        Geolocator.getPositionStream().listen((Position position) async {
      latitude.value = position.latitude.toString();
      longitude.value = position.longitude.toString();
      await storage.write(key: "latitude", value: latitude.toString());
      await storage.write(key: "longitude", value: longitude.toString());

      print((await storage.read(key: "latitude"))!);
      print((await storage.read(key: "longitude"))!);
    });
  }
}
