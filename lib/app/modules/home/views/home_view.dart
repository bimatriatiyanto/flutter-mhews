import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import 'package:latlong2/latlong.dart';
import 'example_popup.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  /// Used to trigger showing/hiding of popups.
  final List<LatLng> _markerPositions = [
    LatLng(44.421, 10.404),
  ];
  final PopupController _popupLayerController = PopupController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HomeView'),
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          mapGempa(),
        ],
      ),
    );
  }

  Widget mapGempa() {
    return Container(
      height: 300,
      child: Card(
        elevation: 5,
        shadowColor: Colors.black,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        margin: EdgeInsets.all(10),
        child: FlutterMap(
          options: MapOptions(
            zoom: 5.0,
            center: LatLng(44.421, 10.404),
            // Hide popup when the map is tapped.
          ),
          children: [
            TileLayerWidget(
              options: TileLayerOptions(
                urlTemplate:
                    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                subdomains: ['a', 'b', 'c'],
              ),
            ),
            PopupMarkerLayerWidget(
              options: PopupMarkerLayerOptions(
                popupController: _popupLayerController,
                markers: _markers,
                markerRotateAlignment:
                    PopupMarkerLayerOptions.rotationAlignmentFor(
                        AnchorAlign.top),
                popupBuilder: (BuildContext context, Marker marker) =>
                    Container(
                  child: ExamplePopup(marker),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Marker> get _markers => _markerPositions
      .map(
        (markerPosition) => Marker(
          point: markerPosition,
          width: 40,
          height: 40,
          builder: (_) => Icon(Icons.location_on, size: 40),
          anchorPos: AnchorPos.align(AnchorAlign.top),
        ),
      )
      .toList();
}
