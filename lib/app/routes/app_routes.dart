part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const GEMPA = _Paths.GEMPA;
  static const LOGIN = _Paths.LOGIN;
  static const CUACA = _Paths.CUACA;
  static const DASHBOARD = _Paths.DASHBOARD;
}

abstract class _Paths {
  static const HOME = '/home';
  static const GEMPA = '/gempa';
  static const LOGIN = '/login';
  static const CUACA = '/cuaca';
  static const DASHBOARD = '/dashboard';
}
